//
//  ServiceName.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 7/9/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import Foundation

// 1. We enumerate our services so we can use a switch statement
enum ServiceName {
    case custom
    case weebly
    case goDaddy
    case iOSApp
    case androidApp
    case mobileApp
    case websiteAndApp
    case logos
}
