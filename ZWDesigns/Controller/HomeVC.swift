//
//  ViewController.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 3/28/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var webBtn: RoundedButton!
    @IBOutlet weak var appBtn: RoundedButton!
    @IBOutlet weak var webAppBtn: RoundedButton!
    @IBOutlet weak var logoBtn: RoundedButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeVC.handleTap))
        view.addGestureRecognizer(tap)
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    @IBAction func webBtnWasPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToWebsiteVC", sender: self)
    }
    
    @IBAction func appBtnWasPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToAppVC", sender: self)
    }
    
    @IBAction func webAppBtnWasPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToWebsiteAppVC", sender: self)
    }
    
    @IBAction func logoBtnWasPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToLogoVC", sender: self)
    }
    
}

