//
//  ProductModalVC.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 6/12/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

class ProductModalVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var productImg: UIImageView!
    
    // Variables
    private(set) public var products = [Product]()
    var productImage = String()
    var productLink = URL(string: "")
    
    // Constants
    let productView = ProductVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    @IBAction func closeModalBtnWasPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func projectViewBtnWasPressed(_ sender: Any) {
        if productLink != nil {
            UIApplication.shared.open(productLink!, options: [:])
        }
    }
    
    func setupView() {
        let closeTouch = UITapGestureRecognizer(target: self, action: #selector(ProductModalVC.closeTap(_:)))
        bgView.addGestureRecognizer(closeTouch)
        productImg.image = UIImage(named: productImage)
    }
    
    @objc func closeTap(_ recognizer: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }

}
