//
//  WebsiteVC.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 4/18/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

class WebsiteVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func closeBtnWasPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func customBuiltBtnWasPressed(_ sender: Any) {
        let servicesVC = ServicesVC(nibName: "ServicesVC", bundle: nil)
        servicesVC.modalPresentationStyle = .custom
        servicesVC.selectedService = DataService.instance.getService(serviceName: .custom)
        present(servicesVC, animated: true, completion: nil)
    }
    
    @IBAction func weeblyBtnWasPressed(_ sender: Any) {
        let servicesVC = ServicesVC(nibName: "ServicesVC", bundle: nil)
        servicesVC.modalPresentationStyle = .custom
        servicesVC.selectedService = DataService.instance.getService(serviceName: .weebly)
        present(servicesVC, animated: true, completion: nil)
    }
    
    @IBAction func goDaddyBtnWasPressed(_ sender: Any) {
        let servicesVC = ServicesVC(nibName: "ServicesVC", bundle: nil)
        servicesVC.modalPresentationStyle = .custom
        servicesVC.selectedService = DataService.instance.getService(serviceName: .goDaddy)
        present(servicesVC, animated: true, completion: nil)
    }
    
    @IBAction func profileBtnWasPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToPortfiloVC", sender: self)
    }

}
