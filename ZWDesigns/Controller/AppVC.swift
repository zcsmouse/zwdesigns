//
//  AppVC.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 4/27/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

class AppVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func closeBtnWasPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func iosBtnWasPressed(_ sender: Any) {
        let servicesVC = ServicesVC(nibName: "ServicesVC", bundle: nil)
        servicesVC.modalPresentationStyle = .custom
        servicesVC.selectedService = DataService.instance.getService(serviceName: .iOSApp)
        present(servicesVC, animated: true, completion: nil)
    }
    
    @IBAction func androidBtnWasPressed(_ sender: Any) {
        let servicesVC = ServicesVC(nibName: "ServicesVC", bundle: nil)
        servicesVC.modalPresentationStyle = .custom
        servicesVC.selectedService = DataService.instance.getService(serviceName: .androidApp)
        present(servicesVC, animated: true, completion: nil)
    }
    
    @IBAction func iosAndroidBtnWasPressed(_ sender: Any) {
        let servicesVC = ServicesVC(nibName: "ServicesVC", bundle: nil)
        servicesVC.modalPresentationStyle = .custom
        servicesVC.selectedService = DataService.instance.getService(serviceName: .mobileApp)
        present(servicesVC, animated: true, completion: nil)
    }
    
    @IBAction func portfolioBtnWasPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToPortfolioVC", sender: self)
    }

}
