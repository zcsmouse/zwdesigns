//
//  ServicesVC.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 5/9/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

class ServicesVC: UIViewController {
    
    // Outlets
    @IBOutlet private weak var bgView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var quoteLabel: UILabel!
    @IBOutlet private weak var mainText: UILabel!
    @IBOutlet private weak var detailsHeaderLabel: UILabel!
    @IBOutlet private weak var detailsFirstLabel: UILabel!
    @IBOutlet private weak var detailsSecondLabel: UILabel!
    @IBOutlet private weak var detailsThirdLabel: UILabel!
    @IBOutlet private weak var detailsFourthLabel: UILabel!
    @IBOutlet private weak var quoteButton: RoundedButton!
    
    // Variables
    private(set) public var services = [Service]()
    var selectedService: Service?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
        guard selectedService != nil else { return }
        
        let details = selectedService?.prices.components(separatedBy: ", ")
        
        self.titleLabel.text = selectedService?.title
        self.quoteLabel.text = selectedService?.quote
        self.mainText.text = selectedService?.packageDetail
        self.quoteButton.setTitle(selectedService?.emailBtn, for: .normal)
        
//        "What do we provide?, Unlimited web hosting, Unlimited updates for life, A free domain for a year, A free logo if needed"
        
        self.detailsHeaderLabel.text = details?[0]
        self.detailsFirstLabel.text = details?[1]
        self.detailsSecondLabel.text = details?[2]
        self.detailsThirdLabel.text = details?[3]
        self.detailsFourthLabel.text = details?[4]
        
    }
    
    @IBAction func closeModalPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func interestedBtnWasPressed(_ sender: Any) {
        presentMailComposer()
    }
    
    func setupView() {
        let closeTouch = UITapGestureRecognizer(target: self, action: #selector(ServicesVC.closeTap(_:)))
        bgView.addGestureRecognizer(closeTouch)
    }
    
    @objc func closeTap(_ recognizer: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    /* (title: "Custom Built", quote: "What it consists of...", packageDetail: "If you are wondering what a custom built website is, here's a little insight. A custom consists of a lot of different code that makes each website unique. Our team will take the website from scratch to something beautiful. We can also create a demo for you before you decide on this service.", prices: , emailBtn: "Get a Free Quote!") */
    
}
