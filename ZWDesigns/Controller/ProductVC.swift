//
//  ProductVC.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 6/7/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

class ProductVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // Oulets
    @IBOutlet weak var productsCollection: UICollectionView!
    
    // Variables
    private(set) public var products = [Product]()

    override func viewDidLoad() {
        super.viewDidLoad()
        productsCollection.delegate = self
        productsCollection.dataSource = self
    }
    
    func initProducts(catergory: Catergory) {
        
        products = DataService.instance.getProducts(forCatergoryTitle: catergory.title)
        navigationItem.title = catergory.title
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as? ProductCell {
            let products = self.products[indexPath.row]
            cell.updateViews(product: products)
            return cell
        }
        return ProductCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productModal = ProductModalVC()
        productModal.modalPresentationStyle = .custom
        present(productModal, animated: true, completion: nil)
        productModal.productImg.image = UIImage(named: products[indexPath.row].imageTitle)
        productModal.productLink = URL(string: "\(products[indexPath.row].websiteTitle)")!
    }
    
}
