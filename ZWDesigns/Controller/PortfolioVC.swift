//
//  PortfolioVC.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 6/6/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

class PortfolioVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Outlets
    @IBOutlet weak var catergoryTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        catergoryTable.delegate = self
        catergoryTable.dataSource = self
    }
    
    @IBAction func closeBtnWasPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getCatergories().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CatergoryCell") as? CatergoryCell {
            let catergory = DataService.instance.getCatergories()[indexPath.row]
            cell.updateViews(catergory: catergory)
            return cell
        }
        return CatergoryCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let catergory = DataService.instance.getCatergories()[indexPath.row]
        performSegue(withIdentifier: "goToProductVC", sender: catergory)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let productVC = segue.destination as? ProductVC {
            let barBtn = UIBarButtonItem()
            barBtn.title = ""
            navigationItem.backBarButtonItem = barBtn
            
            productVC.initProducts(catergory: sender as! Catergory)
        }
    }

}
