//
//  ContactInfoVC.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 4/12/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit
import MessageUI

class ContactInfoVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.width - 60
    }
    
    @IBAction func mailBtnWasPressed(_ sender: Any) {
        presentMailComposer()
    }
    
    @IBAction func phoneBtnWasPressed(_ sender: Any) {
        presentMessageComposer()
    }
    
    @IBAction func websiteBtnWasPressed(_ sender: Any) {
        if let url = URL(string: "http://www.zwdalpha.com") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
}
