//
//  DataService.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 5/28/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import Foundation

class DataService {
    
    
    
    static let instance = DataService()
    
    private let catergories = [
    
        Catergory(title: "WEBSITES", imageName: "05-1.png"),
        Catergory(title: "APPS", imageName: "app.jpg"),
        Catergory(title: "LOGOS", imageName: "logo.jpg")
    
    ]
    
    private let websites = [
    
        Product(title: "Jimmer's BBQ", imageName: "jimmersbbq.png", websiteTitle: URL(string: "http://www.jimmersbbq.com")!),
        Product(title: "Amy's Bookcase", imageName: "amys-bookcase.png", websiteTitle: URL(string: "http://www.amysbookcase1.com")!),
        Product(title: "Panther Press", imageName: "pantherpress.png", websiteTitle: URL(string: "http://www.mchspanthernews.org")!),
        Product(title: "Fairway Physical Therapy", imageName: "fairway.png", websiteTitle: URL(string: "http://www.fairwaysutherlinpt.com")!),
        Product(title: "Glenwood Springs BPOE 2286", imageName: "gwelks.png", websiteTitle: URL(string: "http://www.gwselks.com")!)
    
    ]
    
    private let apps = [Product]()
    
    private let logos = [
    
        Product(title: "Doodle Destinations", imageName: "doodle.png", websiteTitle: URL(string: "http://www.zwdalpha.com")!),
        Product(title: "Northwest Literacy Foundation", imageName: "northwest.png", websiteTitle: URL(string: "http://www.zwdalpha.com")!)
    
    ]
    
    // 2. We create our services and specify all the info
    private let custom = Service(title: "Custom Built", quote: "What it consists of...", packageDetail: "If you are wondering what a custom built website is, here's a little insight. A custom consists of a lot of different code that makes each website unique. Our team will take the website from scratch to something beautiful. We can also create a demo for you before you decide on this service.", prices: "What do we provide?, Unlimited web hosting, Unlimited updates for life, A free domain for a year, A free logo if needed", emailBtn: "Get a Free Quote!")

    private let weebly = Service(title: "Weebly Cloud", quote: "Why choose this service?", packageDetail: "Weebly is a website builder that will open an elegant opportunity for you. It has special features that might open your business up in a whole new way. From having a fancy blog to a delicate business website, we will provide you with everything necessary to grow.", prices: "What you pay?, Pro - $10.54/month or $111.17/year, Business - $23.20/month or $239.23/year, Our Services - $100 upfront, $100 when completed and $100/month for a year", emailBtn: "Contact Us Today!")

    private let goDaddy = Service(title: "GoDaddy", quote: "Simple at Its Best!", packageDetail: "If you are just starting out and want something simple, our GoDaddy plan is for you. No matter how simple you needs, our team will get you from point A to point B smoothly. We can give you a variety of themes to pick from too! Have questions? Please don't hesitate to ask, we are always here to help you.", prices: "What Does it Cost?, Personal - $5.99/month or $71.88/year, Business - $9.99/month or $119.88/year, Business Plus - $14.99/month or $179.88/month, Our Services - $100 plus $50 each additonal page", emailBtn: "Contact Us for More Information!")

    private let iOSApp = Service(title: "iOS App", quote: "Bringing the Power of Apple Apps to you!", packageDetail: "If you are wanting an app only for the App Store, this package is for you! Apple is the number one growing platform for apps and we are a licensed Apple development team. From a small app to a large one,we will make sure you are happy with the app we build you!", prices: "What Price Will it Cost?,  Give us an idea and we will give you a quote, A yearly contract included, Updating varies, We provide our developers license", emailBtn: "E-mail us today!")

    private let androidApp = Service(title: "Android App", quote: "Grow Your Opportunies!", packageDetail: "Our Android service will give you an app if you want it for only the Play Store. Our team will build out your app anyway that you need us to. No matter what kind of app you want, we will make sure your idea will come to life!", prices: "What Price Will it Cost You?, Give us an idea and we will give you a quote, A yearly contract included, Updating varies, We provide our developers license", emailBtn: "Contact Us if Interested!")

    private let mobileApp = Service(title: "iOS/Android App", quote: "Get the Power of Two Apps!", packageDetail: "Think it would be cool to have an app on Apple and Android? Then let our team build the two apps for you! We will take our time to make sure the two apps turn out perfectly for your prohect.", prices: "What Price Will it Cost?, Give us an idea and we will give you a quote, A yearly contract included, Updating varies, We provide our developers license", emailBtn: "Get Your Two Apps Today!")

    private let websiteAndApp = Service(title: "Website/App Combo", quote: "The Perfect Match!", packageDetail: "Needing a website and an app? Let us know and we can go over every detail to make sure the project turns out as you imagined. We can also discuss what platforms is best for you.", prices: "Wondering what price this will be?, We will give you a price quote, A yearly contract included, Updating price quote, We provide anything you need", emailBtn: "Get Your Website and App Started Today!")

    private let logo = Service(title: "Logos", quote: "Putting the Little Zing into your Business Mojo!", packageDetail: "Want the logo that just makes your business stand out a little more? You have come to the right place! Our team will understand your needs and design you the perfect logo.", prices: "Wondering what you have to pay?, $75 upfront, $75 when completed, or $150 when your project is finalized, Unlimited edits", emailBtn: "Contact Us for Your Logo!")
    
    // 3. We use getService and specify a service name (enum comes in handy)
    // We return our constant Service (they share a same name so it's easy to remember
    func getService(serviceName: ServiceName) -> Service {
        
        switch serviceName {
            
        case .custom:
            return custom
        case .weebly:
            return weebly
        case .goDaddy:
            return goDaddy
        case .iOSApp:
            return iOSApp
        case .androidApp:
            return androidApp
        case .mobileApp:
            return mobileApp
        case .websiteAndApp:
            return websiteAndApp
        case .logos:
            return logo
            
        }
        
    }
    
    
    func getCatergories() -> [Catergory] {
        
        return catergories
        
    }
    
    func getProducts(forCatergoryTitle title: String) -> [Product] {
        
        switch title {
        case "WEBSITES":
            return getWebsites()
        case "APPS":
            return getApps()
        case "LOGOS":
            return getLogos()
        default:
            return getWebsites()
        }
        
    }
    
    func getWebsites() -> [Product] {
        
        return websites
        
    }
    
    func getApps() -> [Product] {
        
        return apps
        
    }
    
    func getLogos() -> [Product] {
        
        return logos
        
    }
    
    
}
