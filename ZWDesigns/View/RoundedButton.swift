//
//  RoundedButton.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 4/5/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

@IBDesignable

class RoundedButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        
        didSet {
            
            self.layer.cornerRadius = cornerRadius
            
        }
        
    }
    
    override func awakeFromNib() {
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    func setupView() {
        
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = 1.0
        self.layer.borderColor = #colorLiteral(red: 0.9834352484, green: 0.9780466778, blue: 1, alpha: 1)
        
    }

}
