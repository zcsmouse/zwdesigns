//
//  CatergoryCell.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 6/6/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

class CatergoryCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var catergoryImage: UIImageView!
    @IBOutlet weak var catergoryTitle: UILabel!

    func updateViews(catergory: Catergory) {
        
        catergoryImage.image = UIImage(named: catergory.imageName)
        catergoryTitle.text = catergory.title
        
    }

}
