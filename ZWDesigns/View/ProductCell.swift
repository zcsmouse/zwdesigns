//
//  ProductCell.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 6/6/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    // Outlets
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    
    func updateViews(product: Product) {
        
        productImage.image = UIImage(named: product.imageTitle)
        productTitle.text = product.title
        
    }
    
}
