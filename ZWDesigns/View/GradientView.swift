//
//  GradientView.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 3/28/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit

@IBDesignable

class GradientView: UIView {

    @IBInspectable var topColor: UIColor = #colorLiteral(red: 0.8705882353, green: 0.3058823529, blue: 0, alpha: 1) {
        
        didSet {
            
            self.setNeedsLayout()
            
        }
        
    }
    
    @IBInspectable var bottomColor: UIColor = #colorLiteral(red: 0.003921568627, green: 0.003921568627, blue: 0.003921568627, alpha: 1) {
        
        didSet {
            
            self.setNeedsLayout()
            
        }
        
    }
    
    override func layoutSubviews() {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
        
    }

}
