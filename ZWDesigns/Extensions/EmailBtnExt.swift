//
//  EmailBtnExt.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 4/26/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import Foundation
import MessageUI

extension UIViewController: MFMailComposeViewControllerDelegate {
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func presentMailComposer() {
        if !MFMailComposeViewController.canSendMail() {
            return
        } else {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.setToRecipients(["zwdalpha@gmail.com"])
            composeVC.setSubject("Concerning Your Services Through the iOS App")
            composeVC.setMessageBody("Your Project Details.", isHTML: true)
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
        }
    }
}

