//
//  SendTextBtnExt.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 4/27/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import UIKit
import MessageUI

extension UIViewController: MFMessageComposeViewControllerDelegate {
    public func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func presentMessageComposer() {
        if MFMessageComposeViewController.canSendText() {
            let controller = MFMessageComposeViewController()
            controller.recipients = ["19707592739"]
            controller.body = ""
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        } else {
            print("Couldn't send text.")
        }
    }
    
}
