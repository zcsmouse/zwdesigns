//
//  Catergory.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 6/6/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import Foundation

struct Catergory {
    
    // Variables
    private(set) public var title: String
    private(set) public var imageName: String
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
    
}
