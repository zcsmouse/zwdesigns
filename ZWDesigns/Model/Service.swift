//
//  Service.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 6/23/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import Foundation

struct Service {
    
    // Variables
    private(set) public var title: String
    private(set) public var quote: String
    private(set) public var packageDetail: String
    private(set) public var prices: String
    private(set) public var emailBtn: String
    
    // Struct does not need an initializer! It has a default initializer provided by Swift
//    init(title: String, quote: String, packageDetail: String, prices: String, emailBtn: String) {
//        self.title = title
//        self.quote = quote
//        self.packageDetail = packageDetail
//        self.prices = prices
//        self.emailBtn = emailBtn
//    }
    
}
