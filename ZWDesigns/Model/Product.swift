//
//  Product.swift
//  ZWDesigns
//
//  Created by Zachary Smouse on 5/20/18.
//  Copyright © 2018 Zachary Smouse. All rights reserved.
//

import Foundation

struct Product {
    
    // Variables
    private(set) public var title: String
    private(set) public var imageTitle: String
    private(set) public var websiteTitle: URL
    
    init(title: String, imageName: String, websiteTitle: URL) {
        self.title = title
        self.imageTitle = imageName
        self.websiteTitle = websiteTitle
    }
    
}
